﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
       // Achtung!!!!
       // Hier wird deutlich, warum double nicht für Geldbeträge geeignet ist.
       // =======================================
       //double zuZahlenderBetrag; 
       //double eingezahlterGesamtbetrag;
      // double eingeworfeneMünze;
      // double rückgabebetrag;
       // Die Eingabe erfolgt anwenderfreudlich mit Dezimalpunkt: just testing
       //double eingegebenerBetrag;
       double zuZahlenderBetrag = fahrkartenBerechnen();
       double rückgabebetrag = geldEinwerfen(zuZahlenderBetrag);
       fahrscheinAusgeben();
       rückgeldAusgabe(rückgabebetrag);
       
       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
               "vor Fahrtantritt entwerten zu lassen!\n"+
               "Wir wünschen Ihnen eine gute Fahrt.");
       // Den zu zahlenden Betrag ermittelt normalerweise der Automat
       // aufgrund der gewählten Fahrkarte(n).
       // -----------------------------------
    }
       public static double fahrkartenBerechnen() {
    
       double zuZahlenderBetrag;
       double eingegebenerBetrag;
       
       Scanner tastatur = new Scanner(System.in);
    	
       System.out.print("Zu zahlender Betrag (EURO): ");
       zuZahlenderBetrag = tastatur.nextDouble();
       
       System.out.print("Anzahl der Tickets: ");
       eingegebenerBetrag = tastatur.nextDouble();
       
       zuZahlenderBetrag = zuZahlenderBetrag * eingegebenerBetrag;
       return zuZahlenderBetrag;
       
       }
       // Geldeinwurf
       // -----------
       public static double geldEinwerfen(double zuZahlenderBetrag) {
       
       double eingezahlterGesamtbetrag;
       double eingeworfeneMünze;
       double rückgabebetrag;
       Scanner tastatur = new Scanner(System.in);
       
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	  // System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag));
    	   System.out.printf("Noch zu zahlen: ");
    	   System.out.printf("%.2f Euro\n",  zuZahlenderBetrag - eingezahlterGesamtbetrag);
    	   System.out.print( "Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
           
           
           
       }
       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       return rückgabebetrag;
       }
       // Fahrscheinausgabe
       // -----------------
       
       public static void fahrscheinAusgeben() {
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");
       }
       // Rückgeldberechnung und -Ausgabe eingezahlterGesamtbetrag - zuZahlenderBetrag
       // -------------------------------
       public static void rückgeldAusgabe(double rückgabebetrag) {
       if(rückgabebetrag > 0.0)
       {
    	   //System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
    	   System.out.printf("Der Rückgabebetrag in Höhe von ");
    	   System.out.printf("%.2f EURO ", rückgabebetrag);
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
           }
           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
           {
        	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
           }
           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
           }
           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("20 CENT");
 	          rückgabebetrag -= 0.2;
           }
           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
           }
           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 CENT");
 	          rückgabebetrag -= 0.05;
           }
       }
       }
       
    }
