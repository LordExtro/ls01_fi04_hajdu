import java.util.Scanner;

class FahrkartenExtra
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
       // Achtung!!!!
       // Hier wird deutlich, warum double nicht f�r Geldbetr�ge geeignet ist.
       // =======================================
       //double zuZahlenderBetrag; 
       //double eingezahlterGesamtbetrag;
      // double eingeworfeneM�nze;
      // double r�ckgabebetrag;
       // Die Eingabe erfolgt anwenderfreudlich mit Dezimalpunkt: just testing
       //double eingegebenerBetrag;
       double zuZahlenderBetrag = fahrkartenBerechnen();
       double r�ckgabebetrag = geldEinwerfen(zuZahlenderBetrag);
       fahrscheinAusgeben();
       r�ckgeldAusgabe(r�ckgabebetrag);
       
       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
               "vor Fahrtantritt entwerten zu lassen!\n"+
               "Wir w�nschen Ihnen eine gute Fahrt.");
       // Den zu zahlenden Betrag ermittelt normalerweise der Automat
       // aufgrund der gew�hlten Fahrkarte(n).
       // -----------------------------------
    }
       public static double fahrkartenBerechnen() {
    
       double zuZahlenderBetrag;
       double eingegebenerBetrag;
       
       Scanner tastatur = new Scanner(System.in);
    	
       System.out.print("Zu zahlender Betrag (EURO): ");
       zuZahlenderBetrag = tastatur.nextDouble();
       
       System.out.print("Anzahl der Tickets: ");
       eingegebenerBetrag = tastatur.nextDouble();
       
       while (eingegebenerBetrag > 10 || eingegebenerBetrag < 0) {
    	   
    	   System.out.print("Falscher Betrag.\n");
    	   System.out.print("Anzahl der Tickets: ");
    	   eingegebenerBetrag = tastatur.nextDouble();
           
       }
       zuZahlenderBetrag = zuZahlenderBetrag * eingegebenerBetrag;  
       return zuZahlenderBetrag;
       }
       // Geldeinwurf
       // -----------
       public static double geldEinwerfen(double zuZahlenderBetrag) {
       
       double eingezahlterGesamtbetrag;
       double eingeworfeneM�nze;
       double r�ckgabebetrag;
       Scanner tastatur = new Scanner(System.in);
       
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	  // System.out.println("Noch zu zahlen: " + (zuZahlenderBetrag - eingezahlterGesamtbetrag));
    	   System.out.printf("Noch zu zahlen: ");
    	   System.out.printf("%.2f Euro\n",  zuZahlenderBetrag - eingezahlterGesamtbetrag);
    	   System.out.print( "Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
    	   eingeworfeneM�nze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
           
           
           
       }
       r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       return r�ckgabebetrag;
       }
       // Fahrscheinausgabe
       // -----------------
       
       public static void fahrscheinAusgeben() {
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");
       }
       // R�ckgeldberechnung und -Ausgabe eingezahlterGesamtbetrag - zuZahlenderBetrag
       // -------------------------------
       public static void r�ckgeldAusgabe(double r�ckgabebetrag) {
       if(r�ckgabebetrag > 0.0)
       {
    	   //System.out.println("Der R�ckgabebetrag in H�he von " + r�ckgabebetrag + " EURO");
    	   System.out.printf("Der R�ckgabebetrag in H�he von ");
    	   System.out.printf("%.2f EURO ", r�ckgabebetrag);
    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
           {
        	  System.out.println("2 EURO");
	          r�ckgabebetrag -= 2.0;
           }
           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
           {
        	  System.out.println("1 EURO");
	          r�ckgabebetrag -= 1.0;
           }
           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
           {
        	  System.out.println("50 CENT");
	          r�ckgabebetrag -= 0.5;
           }
           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
           {
        	  System.out.println("20 CENT");
 	          r�ckgabebetrag -= 0.2;
           }
           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
           {
        	  System.out.println("10 CENT");
	          r�ckgabebetrag -= 0.1;
           }
           while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
           {
        	  System.out.println("5 CENT");
 	          r�ckgabebetrag -= 0.05;
           }
       }
       }
       
    }